from diffieHellman import DiffieHellman

Alice = DiffieHellman()
Bob = DiffieHellman()

Bob.receiveMessage(Alice.sendMessage())
Alice.receiveMessage(Bob.sendMessage())

assert Bob.getSecret() == Alice.getSecret()