import hashlib
import os
from bulk_cipher import encrypt, decrypt
import struct
import base64
BLOCK_SIZE = 16

class Agent:
    def __init__(self, msg = None):
        pass

    def send_public_data(self):
        pass

    def receive_public_data(self, p, g):
        pass

    def send_public_key(self):
        pass

    def receive_public_key(self, rec):
        pass

    def send_message(self):
        pass

    def receive_message(self, msg):
        pass