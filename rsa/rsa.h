/**
 * @defgroup   RSA Implements all of the RSA functions
 *
 * @brief      This file implements rsa.
 *
 * @author     
 * @date       2022
 */

#ifndef RSA_H
#define RSA_H

#include <string>
#include <iostream>
#include <stdio.h>
#include <gmpxx.h>
#include <gmp.h>
#include <ctime>
#include <sstream>

using namespace std;

class RSA {
	private:
		mpz_class p, q, n, phi, e, d;
		void inv();
	public:
		void encrypt(mpz_class i_e, mpz_class i_n, string message);
		void decrypt(mpz_class i_d, mpz_class i_n, string message);
		void crack_rsa(mpz_class i_e, mpz_class i_n, string message);
};

#endif