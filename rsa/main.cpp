/**
 * @defgroup   MAIN main
 *
 * @brief      This file implements main functions and CLI parsing.
 *
 * @author     
 * @date       2022
 */

#include <iostream>
#include <unistd.h>
#include <sstream>

#include "rsa.h"

using namespace std;

string remove_base(string number);

int main(int argc, char **argv) {
	RSA rsa;
	mpz_class e;
	mpz_class n;
	mpz_class d;
	string m = "";
	stringstream ss1;
	stringstream ss2;
	argc = argc;

 	if(string(argv[1]) == "-e") {
		ss1 << hex << remove_base(argv[2]).c_str();
		ss1 >> e;
		ss2 << hex << remove_base(argv[3]).c_str();
		ss2 >> n;
		if(e == mpz_class(0) || n == mpz_class(0)) {
			cerr << "Invalid arguments for encryption!" << endl;
			return -1;
		}

		m = remove_base(argv[4]);

		#ifdef VERBOSE
			cout << "e: " << e << endl;
			cout << "n: " << n << endl;
			cout << "m: " << m << endl;
		#endif
		rsa.encrypt(e, n, m);
	} else if(string(argv[1]) == "-d") {
		ss1 << hex << remove_base(argv[2]).c_str();
		ss1 >> d;
		ss2 << hex << remove_base(argv[3]).c_str();
		ss2 >> n;
		m = remove_base(argv[4]);

		if(d == mpz_class(0) || n == mpz_class(0)) {
			cerr << "Invalid arguments for decryption!" << endl;
			return -1;
		}

		#ifdef VERBOSE
			cout << "d: " << d << endl;
			cout << "n: " << n << endl;
			cout << "m: " << m << endl;
		#endif
		rsa.decrypt(d, n, m);
	} else if(string(argv[1]) == "-b") {
		ss1 << hex << remove_base(argv[2]).c_str();
		ss1 >> e;
		ss2 << hex << remove_base(argv[3]).c_str();
		ss2 >> n;

		if(e == mpz_class(0) || n == mpz_class(0)) {
			cerr << "Invalid arguments for cracking the RSA algorithm!" << endl;
			return -1;
		}

		m = remove_base(argv[4]);;

		#ifdef VERBOSE
			cout << "e: " << e << endl;
			cout << "n: " << n << endl;
			cout << "m: " << m << endl;
		#endif
		rsa.crack_rsa(e, n, m);
	} else {
		cerr << "Invalid arguments!" << endl;
	}

	return 0;
}

string remove_base(string number) {
	if(number.at(0) == '0' && number.at(1) == 'x') {
		return number.substr(2, number.length());
	}

	return number;
}
