/**
 * @defgroup   RSA Implements all of the RSA functions
 *
 * @brief      This file implements rsa.
 *
 * @author     
 * @date       2022
 */

#ifndef RSA_CPP
#define RSA_CPP

#include "rsa.h"

/**
 * @brief      Calculates the multiplicative inverse
 */
void RSA::inv() {
}

/**
 * @brief      Encrypts the message using the RSA algorithm
 *
 * @param[in]  i_e      e
 * @param[in]  i_n      n
 * @param[in]  message  The message to encrypt
 */
void RSA::encrypt(mpz_class i_e, mpz_class i_n, string message) {
	e = i_e;
	n = i_n;
	string hex_string = message;
	mpz_class message_number;
	mpz_t encrypted_message;
	stringstream ss;

	ss << hex << hex_string;
	ss >> message_number;

	mpz_init(encrypted_message);
	mpz_powm(encrypted_message, message_number.get_mpz_t(), e.get_mpz_t(), n.get_mpz_t());
	cout << hex << "0x" << encrypted_message << endl;

	mpz_clear(encrypted_message);
}

/**
 * @brief      Decrypts the message using the RSA algorithm
 *
 * @param[in]  i_d      d
 * @param[in]  i_n      n
 * @param[in]  message  The message do decrypt
 */
void RSA::decrypt(mpz_class i_d, mpz_class i_n, string message) {
    cout << hex << "0x" << decrypted_message << endl;
}

/**
 * @brief      Function to break the RSA algorithm. Fermat's factorization
 *             is used to find the factors of n, and the values of p and q.
 *
 * @param[in]  i_e      e
 * @param[in]  i_n      n
 * @param[in]  message  encrypted message, that should be desciphered
 */
void RSA::crack_rsa(mpz_class i_e, mpz_class i_n, string message) {
    cout << hex << "0x" << p << " 0x" << q << " 0x" << decrypted_message << endl;
}
#endif
