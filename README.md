# TLS protocol
The goal of this lab is to get familiar with underlying principals of SSL/TLS cryptographic protocol.

## Task 1: Diffie–Hellman key exchange
Implement the [vanilla](https://en.wikipedia.org/wiki/Diffie–Hellman_key_exchange#Cryptographic_explanation) DH algorithm.
Try it with ``p=37`` and `g=5`. Can you make it working with recommended values 
``p=0xFFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA18217C32905E462E36CE3BE39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9DE2BCBF6955817183995497CEA956AE515D2261898FA051015728E5A8AACAA68FFFFFFFFFFFFFFFF``
and ``g=2`` ?

Turn in your code in the folder: `diffie-hellman`

What is your personal shared secret? Submit your answer in a text file that is named: `T1_login.txt`

## Task 2: Diffie–Hellman key 
Turn a DH secret into a key. Use ``sha1`` to generate `BLOCK_SIZE = 16` long key material.

Use the secret key from `Task 1` and apply ``sha1`` with the `BLOCK_SIZE = 16` to generate your personalized secret key.

Submit your personalised key: `T2_login.txt`

Turn in your code in the folder: `tls-setup`

## Task 3: Bulk cipher
Ensure you have working implementation of AES in CBC mode with PKCS&#35;7 padding. It is recommended to use  `BLOCK_SIZE = 16`
You will need ``encrypt(key, iv, message)`` and `decrypt(key, iv, encrypted_message)` functions. 
You can check your implementation with ``bulk_cipher.py`` example.

Fill in the functions in the ``bulk_cipher.py`` sample code and use the ``from Crypto.Cipher import AES`` library for AES encryption.

Turn in your code in the folder: `bulk-cipher`

## Task 4: Implement simple SSL/TLS setup
It's time to have some fun now. Checkout `tls101.py` example. Implement `Agent()` class such that this code executes with no errors.
Re-use the code from Task 1, Task 2 and Task 3 where you implemented parts of the functionality. 
The interface for the ``Agent()`` class should support:
* sending/receiving public data (`p` and `g`)
* sending/receiving public key
* sending/receiving messages

Turn in your code in the folder: `tls-setup`


## Task 5: RSA
[RSA algorithm](https://en.wikipedia.org/wiki/RSA_(cryptosystem)#Key_generation) is the most used asymmetric encryption algorithm in the world. It is based on the principal that it is easy to multiply large numbers, but factoring large numbers is very hard.
Within the TLS context it is used for both key exchange and generate signatures for security certificates (do you know why is that possible?). Let us implement this algorithm.
Here are a few hints:
* The encryption step of the algorithm is already implemented. Look at this code to understand how to work with the `gmp` library that is used to work with very large numbers.
* The program accepts all of the parameters as HEX numbers

Steps to compile:
* make
* Encryption: `./kry -e e n message` for example: `./kry -e 0xbe59504bcae6ec37036eeb75e391 0xc8c783b82331bec590d6a848f5b9 0x1234567890`
* Decryption: `./kry -d d n message` for example: `./kry -d 0xc422d3f295dff98e8b5981a53071 0xc8c783b82331bec590d6a848f5b9 0x9d965c57682bcdffc3aaf092ac28`
* Your personalized key is in the following format: `p q n e d`

What is the output of the program if you run it with your personalized key and message? Submit your answer in a text file that is named: `T5_login.txt`

Turn in your code in the folder: `rsa`

## Task 6:  RSA number factorization
As mentioned in Task 6 large number facorization is hard. But how hard is it? If the size of these numbers is not large enough the RSA algorithm is crackable. Implement the function `crack_rsa`. 

Here are a few hints:
* Look at the `Fermat's Factorization Algorithm`
* You have all the tools to test your algorithm, keys shorter than 64bits can be cracked quickly
* Your personalized key is in the following format: `e n message`

Steps to compile and run:
* make
* Cracking: `./kry -b e n message` for example: `./kry -b 0xa5c939df4daaa1cd 0xd7b5c18afaafbd0d 0x4070531d408ae77a`

Turn in your code in the folder: `rsa`

What is the output of the program if you run it with your personalized key and message? Submit your answer in a text file that is named: `T6_login.txt`

## Task 7:  RSA broadcast attack
It's time to check now that despite a really complex math involved in RSA algorithm it is still might be vulnerable to another type of attack.
In this exercise we will implement the RSA broadcast attack (a.k.a simplest form of [Håstad's broadcast attack](https://en.wikipedia.org/wiki/Coppersmith's_attack#Håstad's_broadcast_attack)) 
Assume yourself an attacker who was lucky enough to capture any 3 of the ciphertexts and their corresponding public keys.
Check out `message_captured`. You also know that those ciphers a related to the same message. Can you read the message? Here are a few hints for this exercise:
* The data is encrypted using `encrypt_int(public, bytes2int(message.encode()))`. 
* Please note, that in all 3 case public keys _are different_

Turn in your code in the folder: `rsa-broadcast`

How Chinese remainder theorem is helping you here? Submit your answer in a text file that is named: `T7_login.txt`

## Folder structure for turning in the results

- diffie-hellman
	- Your code
- tls-setup
	- Your code
- bulk-cipher
	- Your code
- tls-setup
	- Your code
- rsa
	- Your code
- rsa-broadcast
	- Your code
- results
	- T1_login.txt
	- T2_login.txt
	- T5_login.txt
	- T6_login.txt
	- T7_login.txt