import hashlib
import os
from bulk_cipher import encrypt, decrypt
import struct
import base64
from agent import Agent
from agent import fast_mod_exp

BLOCK_SIZE = 16

class MITM:
	def __init__(self, msg=None):
		pass

	def send_public_data(self):
		pass

	def receive_public_data(self, p, g):
		pass

	def send_public_key(self):
		pass

	def receive_public_key(self, rec):
		pass

	def send_message(self, key_n):
		pass

	def receive_message(self, msg, key_n):
		pass

	def intercept_message(self, msg):
		pass





alice = Agent("I'M 5UppER Kewl h4zKEr")
bob = Agent()
mallory = MITM()

# Alice has da message, Bob doesn't
assert alice.msg
assert not bob.msg

# Negotiate parameters publicly
mallory.receive_public_data(*alice.send_public_data())
bob.receive_public_data(*mallory.send_public_data())
mallory.receive_public_data(*bob.send_public_data())
alice.receive_public_data(*mallory.send_public_data())

# Exchange keys publicly
mallory.receive_public_key(alice.send_public_key())
bob.receive_public_key(mallory.send_public_key())
mallory.receive_public_key(bob.send_public_key())
alice.receive_public_key(mallory.send_public_key())

# Pass da message
bob.receive_message(mallory.intercept_message(alice.send_message()))
# Bob has it now
assert bob.msg == alice.msg
# Mallory too
assert mallory.msg == alice.msg

print(mallory.msg)